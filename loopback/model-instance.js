"use strict";
exports.__esModule = true;
var ModelInstance = /** @class */ (function () {
    function ModelInstance() {
    }
    ModelInstance.prototype["delete"] = function (options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.destroy = function (options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.getConnector = function () {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.isNewRecord = function () {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.patchAttributes = function (data, options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.reloadreload = function (cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.remove = function (options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.replaceAttributes = function (data, options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.save = function (options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.setAttribute = function (name, value) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.setAttributes = function (data) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.unsetAttribute = function (name, nullify) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.updateAttribute = function (name, value, options, cb) {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    ModelInstance.prototype.toJSON = function () {
        throw new Error("This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only");
    };
    return ModelInstance;
}());
exports.ModelInstance = ModelInstance;
