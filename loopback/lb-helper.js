"use strict";
exports.__esModule = true;
var LBHelper = /** @class */ (function () {
    function LBHelper() {
    }
    LBHelper.setRemoteMethod = function (model, name, method) {
        model[name] = method;
    };
    LBHelper.setHeadersOptions = function (model, headerProps) {
        model.createOptionsFromRemotingContext = function (ctx) {
            var base = model.base.createOptionsFromRemotingContext(ctx);
            headerProps.forEach(function (headerProp) {
                if (ctx.req.headers[headerProp.header]) {
                    base[headerProp.property] = JSON.parse(ctx.req.headers[headerProp.header]);
                }
                else {
                    base[headerProp.property] = null;
                }
            });
            return base;
        };
    };
    return LBHelper;
}());
exports.LBHelper = LBHelper;
var HeaderProperty = /** @class */ (function () {
    function HeaderProperty() {
    }
    return HeaderProperty;
}());
exports.HeaderProperty = HeaderProperty;
