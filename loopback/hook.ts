export enum Hook {
    access = "access",
    beforeSave = "before save",
    afterSave = "after save",
    beforeDelete = "before delete",
    afterDelete = "after delete",
    loaded = "loaded",
    persist = "persist"
  }