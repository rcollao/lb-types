"use strict";
exports.__esModule = true;
var Hook;
(function (Hook) {
    Hook["access"] = "access";
    Hook["beforeSave"] = "before save";
    Hook["afterSave"] = "after save";
    Hook["beforeDelete"] = "before delete";
    Hook["afterDelete"] = "after delete";
    Hook["loaded"] = "loaded";
    Hook["persist"] = "persist";
})(Hook = exports.Hook || (exports.Hook = {}));
