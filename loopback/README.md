# Installation
> `npm install --save @types/loopback`

# Summary
This package contains type definitions for Loopback ( https://github.com/strongloop/loopback ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/loopback

Additional Details
 * Last updated: Wed, 13 Feb 2019 21:04:27 GMT
 * Dependencies: @types/express-serve-static-core, @types/express
 * Global values: none

# Credits
These definitions were written by Andres D Jimenez <https://github.com/kattsushi>, Tim Schumacher <https://github.com/enko>, Sequoia McDowell <https://github.com/sequoia>, Mike Crowe <https://github.com/drmikecrowe>, Karim Alibhai <https://github.com/karimsa>.
