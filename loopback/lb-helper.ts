export class LBHelper {
	static setRemoteMethod(model: any, name: string, method: any) {
		(model as any)[name] = method;
	}
	static setHeadersOptions(model: any, headerProps: HeaderProperty[]) {
		model.createOptionsFromRemotingContext = (ctx: any) => {
			var base = model.base.createOptionsFromRemotingContext(ctx);
			headerProps.forEach(headerProp => {
				if(ctx.req.headers[headerProp.header]){
                    base[headerProp.property] = JSON.parse(ctx.req.headers[headerProp.header]);
                }else{
                    base[headerProp.property] = null
                }
			});
			return base;
		};
	}
}

export class HeaderProperty {
	property: string;
	header: string;
}
