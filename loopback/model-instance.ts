export class ModelInstance<T> {
  delete(options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  destroy(options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  getConnector(): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  isNewRecord(): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  patchAttributes(data: any, options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  reloadreload(cb: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  remove(options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  replaceAttributes(data: any, options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  save(options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  setAttribute(name: string, value: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  setAttributes(data: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  unsetAttribute(name: any, nullify: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  updateAttribute(name: any, value: any, options?: any, cb?: any): any {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
  toJSON(): T {
    throw new Error(
      "This method is only available when the instance is a loopback Model Instance!. This class is for TYPE reference only"
    );
  }
}
