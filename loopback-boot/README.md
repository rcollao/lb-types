# Installation
> `npm install --save @types/loopback-boot`

# Summary
This package contains type definitions for Loopback-boot (https://github.com/strongloop/loopback-boot).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/loopback-boot

Additional Details
 * Last updated: Mon, 30 Oct 2017 19:05:37 GMT
 * Dependencies: loopback
 * Global values: none

# Credits
These definitions were written by Andres D Jimenez <https://github.com/kattsushi>.
